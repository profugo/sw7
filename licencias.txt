Este proyecto fué liberado bajo licencia GNU GPL (https://www.gnu.org/licenses/licenses.es.html#GPL), bueh, lo que yo hice al menos, el resto:

Sonidos
* over.ogg: GFX Sounds (https://www.youtube.com/watch?v=9IcGDIk4i8s)
* Música: FMA (https://freemusicarchive.org/music/Damiano_Baldoni/Old_Beat/unreachable)
* Las otras voces las hice yo

Gráficos
* nave.png,  krinng.png: Open Game Art (https://opengameart.org/content/top-down-space-ships)
* Imágen de portada: Art Station (https://www.artstation.com/artwork/0nOewE)
* Los otros gráficos son invento mio

Fuentes:
* Airbore GP: 1001 Fonts (https://www.1001fonts.com/airborne-gp-font.html)
* Orbitron Medium: Dafont Free (https://www.dafontfree.net/orbitron-medium/f92274.htm)

