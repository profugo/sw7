'Los metodos  (funciones y subrutinas) que empiezan con guion bajo (_) son nativos de QB64, ver ayuda del IDE mismo

' Hace que los indices de los arrays empiecen en 1 en vez de en 0
Option Base 1

' Ventana principal de 640x480 a 32 bits por pixel
Screen _NewImage(640, 480, 32)

' Variables compartidas por toda la aplicacion (shared)
' Variables exclusivas de esta version
Dim Shared multiple&, portada&, dlgtorpedoes&, gfxcirc&, dlgokcancel&
Dim Shared titlefont&, midfont&, Xofs, Vista, Bases, Cur%, Target

' Variables compartidas del juego original
Dim Shared SC$, KL, BL, I, SD, Red, Q1, Q2, S1, S2, KS, SH, TU, CS, RK, XG, YG, LY, U, RF, E, WP, TC, MAXSP
Dim Shared GL(7, 7), SC(7, 7), K(3, 3), D$(9), D(9), HDR$, C$, A1, B1, X, Y

' Calculo Pi asi paramas presicion
Const Pi = Atn(1) * 4
' CX se usa para convertir grados a radianes
Const CX = Pi / 180
Const build = "220722.1"

' Tipo usado para el calculo de vectores a naves enemigas
Type Enemy
    m As Single
    an As Double
End Type

' Inicializa el contador de danio (sin enie aca) valor 0 a todos los componentes
For a = 1 To 9: D(a) = 0: Next

' Carga de archivis
'Primero carga elementos graficos en memoria, guarda punteros para acceder facil a graficos
mainbg& = _LoadImage("Gfx\Pantalla.png")
multiple& = _LoadImage("Gfx\multiple.png")
portada& = _LoadImage("Gfx\portada.png")
gfxnave& = _LoadImage("Gfx\nave.png")
gfxkrinng& = _LoadImage("Gfx\krinng.png")
gfxstar& = _LoadImage("Gfx\estrella.png")
gfxbase& = _LoadImage("Gfx\base.png")
dlgtorpedoes& = _LoadImage("Gfx\torpedos.png")
gfxcirc& = _LoadImage("Gfx\circ.png")
dlgokcancel& = _LoadImage("Gfx\dlgokcancel.png")
dlgslider& = _LoadImage("Gfx\slider.png")
dlgcourse& = _LoadImage("Gfx\GetCourse.png")

' Luego carga los audios, tambi�en guarda punteros
aud01& = _SndOpen("SOUNDS\STAUD01.ogg", "SYNC,VOL")
aud02& = _SndOpen("SOUNDS\STAUD02.ogg", "SYNC,VOL")
aud03& = _SndOpen("SOUNDS\STAUD03.ogg", "SYNC,VOL")
aud04& = _SndOpen("SOUNDS\STAUD04.ogg", "SYNC,VOL")
aud05& = _SndOpen("SOUNDS\STAUD05.ogg", "SYNC,VOL")
aud06& = _SndOpen("SOUNDS\STAUD06.ogg", "SYNC,VOL")
aud07& = _SndOpen("SOUNDS\STAUD07.ogg", "SYNC,VOL")
aud09& = _SndOpen("SOUNDS\STAUD09.ogg", "SYNC,VOL")
aud10& = _SndOpen("SOUNDS\STAUD10.ogg", "SYNC,VOL")
aud11& = _SndOpen("SOUNDS\STAUD11.ogg", "SYNC,VOL")
aud12& = _SndOpen("SOUNDS\STAUD12.ogg", "SYNC,VOL")
aud14& = _SndOpen("SOUNDS\STAUD14.ogg", "SYNC,VOL")
aud15& = _SndOpen("SOUNDS\STAUD15.ogg", "SYNC,VOL")

msg01& = _SndOpen("SOUNDS\STMSG01.ogg", "SYNC,VOL")
msg02& = _SndOpen("SOUNDS\STMSG02.ogg", "SYNC,VOL")
over& = _SndOpen("SOUNDS\over.ogg", "SYNC,VOL")
bgm& = _SndOpen("SOUNDS\Damiano Baldoni - unreachable.ogg")

' Finalmente carga tipografias
mainfont& = _LoadFont("Fuentes\Orbitron-Medium.ttf", 12)
midfont& = _LoadFont("Fuentes\Orbitron-Medium.ttf", 16)
bigfont& = _LoadFont("Fuentes\Orbitron-Medium.ttf", 32)

titlefont& = _LoadFont("Fuentes\AIRBORNE GP.ttf", 48)

' ReadINI es una funcion mia que sirve para leer tuplas de un archivo de texto, devuelve un string
'
' Formato:
'
' ReadINI(<Archivo>, <Grupo>, <Clave>)
VK = Val(ReadINI("settings.ini", "SW7", "VKeyb"))

Randomize Timer

' Centra en pantalla la ventana de vista
_ScreenMove _Middle

DoIntro

' Limpiar buffer de entrada
salida = 0
Do
    ' Ver que tiene que decir el dispositivo apuntador (mouse, lapiz, touch, etc)
    Do While _MouseInput
    Loop
    salida = 1
    ' Si hay una tecla pulsada o actividad en el dispositivo apuntador esperar
    If (InKey$ <> "") Or (_MouseButton(1) <> 0) Then salida = 0

Loop Until salida = 1
WaitClick

_AutoDisplay

_Font mainfont&

' Reproducir audio de inicio
_SndPlay aud01&

' Poner el gráfico de fondo en la vista
_PutImage (0, 0), mainbg&

' Dibujar estrellas, grillas básicas y guarda en buffers
DrawScr

' Título de la ventana
_Title "Space Wars 7"
' Mostrar la vista en pantalla
_Display

' Esperar a que termine el audio
While _SndPlaying(aud01&)
    _Limit 30
Wend

' Inicializar juego
Init

' Y aca empieza el bucle principal
Principal:

Vista = 0
a = 0
salida = 0
While salida = 0
    ' Primero lee al dispositivo apuntador
    Do While _MouseInput
    Loop

    ' Luego lee el teclado
    k$ = InKey$

    ' Si se hizo clic
    If _MouseButton(1) <> 0 Then
        ' Primero toma las coordenadas de ventana de donde se hizo clic
        X = _MouseX
        Y = _MouseY

        ' En funci�on de donde haces clic se asigna el flag de accion
        If Y > 441 And Y < 480 Then
            If X > 559 And X < 600 Then a = 12
            If X > 600 And X < 640 Then a = 13
        Else
            If (X > 490 And X < 630) Then
                Select Case Y
                    Case 276 TO 292
                        a = 9
                    Case 299 TO 316
                        a = 10
                    Case 321 TO 338
                        a = 11
                End Select
            End If
            If (X > 465 And X < 633) Then
                Select Case Y
                    Case 23 TO 43
                        a = 1
                    Case 51 TO 71
                        a = 2
                    Case 78 TO 98
                        a = 3
                    Case 105 TO 125
                        a = 4
                    Case 132 TO 152
                        a = 5
                    Case 159 TO 179
                        a = 6
                    Case 186 TO 206
                        a = 7
                    Case 214 TO 234
                        a = 8
                End Select
            End If

        End If
        ' Limpiar el buffer del dispositivo apuntador
        Do
            I = _MouseInput
        Loop Until Not _MouseButton(1)

    End If

    ' Si se pulsa una tecla se asigna el flag de accion si es una tecla asociada
    If k$ <> "" Then
        k$ = LCase$(k$)
        a = InStr("12345678abcs", k$)
    End If

    Select Case k$
        Case Is = Chr$(27)
            a = 13
    End Select

    ' Si se asigno el flag de accion hacer cosas
    If a > 0 Then
        If a < 3 Then ClearScr 1

        upd = 0
        refresh = 0
        Select Case a

            ' Radar de corto alcance
            Case Is = 1
                _SndPlay aud02&
                DoRca
                Vista = 1
                upd = 1

                ' Radar de largo alcannce
            Case Is = 2
                _SndPlay aud03&
                DoRla
                Vista = 2
                upd = 1

                ' Disparar torpedos
            Case Is = 3
                _SndPlay aud04&
                Torpedoes
                refresh = 1

                ' Disparar phasers
            Case Is = 4
                _SndPlay aud05&
                Phasers
                refresh = 1

                ' Escudos
            Case Is = 5
                _SndPlay aud06&
                Shields

                ' Unidad Warp
            Case Is = 6
                _SndPlay aud07&
                j = WarpDrive
                If j = 0 Then
                    If ((Cur% = 1) + (Cur% = 2) + (Cur% = 9)) = 0 Then DoPos 1
                    refresh = 1
                End If
                k$ = ""

                ' Reporte de danios
            Case Is = 7
                _SndPlay aud09&
                Vista = 0
                DamageRep

                ' Control de danios
            Case Is = 8
                Vista = 0
                _SndPlay aud10&
                DamageFix

                ' Mostrar carta estelar
            Case Is = 9
                _SndPlay aud11&
                ShowChart
                Vista = 0
                upd = 1

                ' Mostrar estado / posicion
            Case Is = 10
                DoPos 0

                ' Autodestruccion
            Case Is = 11
                _SndPlay aud15&
                SelfDestruct

                ' Configurar teclado virtual
            Case Is = 12
                SetVk

                ' Salir del juego
            Case Is = 13
                ' Guarda la ventana visible en el buffer 4 antes de redibujar
                SaveScr 4

                ' Dibuja el dialogo de cerrar juego
                ShowOkCancel "CERRAR JUEGO"
                PutLine 10, 8, 255, 255, 255, "SALIR DEL JUEGO"
                ' Volcar a pantalla la vista activa
                _Display

                ' Prepara estado predeterminado y espera pulsacion de tecla o clic en control
                E = 10
                DialogClose

                If E = 10 Then System
                ' Restaura la vista guardada al principio cerrando la vista
                ClearScr 4
        End Select

        ' Si el control es refrescable, guardar el comando en la variable Cur%
        If upd > 0 Then
            Cur% = a
        End If

        ' Si hay que refrescar la vista...
        If refresh > 0 Then
            ' Si el comando anterior es refrescable, refrescarlo
            Select Case Cur%
                Case Is = 1
                    DoRca
                Case Is = 2
                    DoRla
                Case Is = 9
                    ShowChart
            End Select

        End If
        a = 0
    End If

Wend

' --------------------------------------------------------------------------------
' -- Radar de corto alcance
' --------------------------------------------------------------------------------
Sub DoRca
    ' Variables globales a las que accede la subrutina
    Shared mainfont&, midfont$
    Shared gfxnave&, gfxkrinng&, gfxstar&, gfxbase&, SC()
    Shared SD, TU, SH, En, TP, KS, K(), D(), HDR$, BL

    ' Primero mira si el RLA esta averiado y si lo esta muestra aviso y aborta la subrutina
    If D(1) < 0 Then
        HDR$ = "AVERIA"
        ShowHelp "EL RADAR DE CORTO ALCANCE ESTA AVERIADO"
        ' Esperar clic o pulsacion de tecla
        WaitClick
        ClearScr 1
        Exit Sub
    End If

    ' Pone en pantalla una grilla vacia de 7x7
    ClearScr 1

    ' Bucle que recorre todos los sectores del cuadrante (a=fila, b=columna)
    For a = 1 To 7
        ' Calcula posicion en coordenadas de pantalla de la fila (eje Y)
        Y = (a * 64) - 57
        For B = 1 To 7
            ' Calcula posicion en coordenadas de pantalla de la columna (eje X)
            X = (B * 64) - 57
            ' Analiza el cuadrante actuak
            Select Case SC(a, B)
                Case Is = 1
                    ' Dibuja nuestra nave
                    _PutImage (X, Y)-(X + 62, Y + 62), gfxnave&
                Case Is = 2
                    ' Dibuja una estrella
                    _PutImage (X, Y)-(X + 62, Y + 62), gfxstar&
                Case Is = 3
                    ' Dibuja una nave enemiga
                    _PutImage (X, Y)-(X + 62, Y + 62), gfxkrinng&
                Case Is = 4
                    ' Dibuja una base aliada
                    _PutImage (X, Y)-(X + 62, Y + 62), gfxbase&
            End Select
    Next B, a

    ' Guarda en el buffer 2la pantalla recien dibujada
    SaveScr 2

    ' Pone una mascara a toda la pantalla bloqueando controles principales
    Mask
    ShowDialog "RADAR CORTO"
    ShowPos
    krinngs 12

    PutLine 200, 2, 255, 255, 255, "TIEMPO: " + Str$(P2(SD - TU))

    PutTxt 4, "ESCUDOS:" + Str$(P2(SH))
    PutTxt 5, "ENERGIA:" + Str$(P2(En))
    PutTxt 6, "TORPEDOS:" + Str$(P2(TP))

    PutLine 240, 5, 255, 255, 255, "BASES:" + Str$(BL)

    If KS > 0 Then
        _Font midfont&
        PutLine 59, 10, 64, 0, 0, "CONDICION ROJA"
        PutLine 61, 10, 64, 0, 0, "CONDICION ROJA"
        PutLine 60, 10, 255, 0, 0, "CONDICION ROJA"
    End If

    _Display

    DialogClose
    ClearScr 2
End Sub


' --------------------------------------------------------------------------------
' -- Mostrar Estado / Posicion
' --------------------------------------------------------------------------------
' Si se pasa un 0 en mute se reproduce el audio
Sub DoPos (mute As Integer)
    Shared aud12&, dialog, SD, TU, SH, En, TP, Q1, Q2, S1, S2, KS, D(), D$(), mainfont&

    ' Guarda pantalla en buffer
    SaveScr 2
    ' Enmascara pantalla para bloquear controles
    Mask
    ' Muestra ventana de estado / posición
    ShowDialog "ESTADO / POSICION"

    PutLine 200, 2, 255, 255, 255, "TIEMPO: " + Str$(P2(SD - TU))

    If D(7) >= 0 Then
        PutTxt 4, "ESCUDOS:" + Str$(P2(SH))
        PutTxt 5, "ENERGIA:" + Str$(P2(En))
        PutTxt 6, "TORPEDOS:" + Str$(P2(TP))
        PutLine 240, 5, 255, 255, 255, "BASES:" + Str$(BL)

        PutTxt 9, "CUADRANTE (" + Str$(Q2) + "," + Str$(Q1) + ")"
        PutTxt 10, "SECTOR (" + Str$(S2) + "," + Str$(S1) + ")"

        ShowPos

        If KS <> 0 Then
            _Font mainfont&
            PutLine 109, 13, 64, 0, 0, "CONDICION ROJA"
            PutLine 111, 13, 64, 0, 0, "CONDICION ROJA"
            PutLine 110, 13, 255, 0, 0, "CONDICION ROJA"
        End If

        krinngs 12

    Else
        PutTxt 8, "COMPUTADORA AVERIADA"
        PutTxt 9, "NO HAY ESTADO / POSICION"
    End If
    _Display

    If mute = 0 Then _SndPlay aud12&
    DialogClose
End Sub

' --------------------------------------------------------------------------------
' -- Inicializa juego
' --------------------------------------------------------------------------------
Sub Init
    Shared midfont&, mainfont&, msg01&, msg02&, SC$, d$, KS, SC()
    Shared Q1, Q2, S1, S2, SH, d, En, TP, SD, TU, RK, I, YG
    Shared dlgslider&, VK, MAXSP, Bases


    Mask

    _Font mainfont&
    ShowOkCancel "SEGURIDAD"

    PutLine 10, 4, 255, 255, 0, "INGRESE CODIGO DE SEGURIDAD"
    _SndPlay msg01&

    ' Si hay que mostrar teclado virtual mostrarlo y si no
    ' hacer un input normal
    If VK = 1 Then
        SC$ = ShowVK$(10, 6, "?> ", 1)
        If E = 10 Then System
    Else

        _Display

        _Font mainfont&
        Locate 6, 10
        Color _RGB(255, 255, 255), 0

        Input "?> ", SC$
    End If

    While _SndPlaying(msg01&): Wend

    rango = 0
    While (rango < 1) Or (rango > 12)
        ClearScr 1

        Mask

        _Font midfont&

        _PutImage (0, 7), dlgslider&
        Color _RGB(255, 255, 255), 0

        Xofs = 30
        PutLine 50, 5, 255, 255, 255, "1 - 12"
        Xofs = 20
        PutTxt 4, "RANGO:"

        PutLine 200, 4, 255, 255, 0, "INGRESE RANGO"

        _Font mainfont&
        PutLine 10, 2, 255, 255, 255, "RANGO"
        PutLine 200, 8, 255, 255, 255, "?> " + Str$(rango) + "_"

        ' Reproduce audio "Introdoce rango, de 1 a 12"
        _SndPlay msg02&
        _Display
        E = 0
        Do
            _Limit 30
            ' Aceptar clics en slider o números de teclado
            rango = ProcessNumeric(12, rango)
            ' Si se cancela la variable de estado E pasa a valer 11
            If E = 11 Then System
            ' Si no cancela limpia texto y lo redibuja
            Line (200, 83)-(322, 98), _RGB(0, 0, 0), BF

            Xofs = 200
            PutTxt 8, "?> " + Str$(rango) + "_"
            _Display
            ' Si hay estado y el rango vale 0, eliminar el estado y
            ' volver a iterar
            If E <> 0 And rango = 0 Then E = 0
        Loop While E = 0
        ClearScr 1
    Wend

    While _SndPlaying(msg02&): Wend
    ClearScr 1
    Mask

    ' Casi todos los valores son iguales al original (ver ref a libro en manual)
    RF = rango

    I = RF * 20 ' Factor de riesgo
    RK = RF
    RF = RF + .0001

    XA = Int(RK * 50) ' Relación entre energía y dificultad (factor de riesgo)
    MAXSP = P3(10 / (RK + .1)) ' Velocidad máxima en función del factor de riesgo
    En = 3100 - XA ' Energía máxima reservable
    SH = 500 + Int(RF * 125) ' 1500/12=12, es decir, en la maxima dificultad tendras 2000 de escudos
    TP = 10 ' Torpedos
    Q1 = RD ' Fila en la galaxia, aleatorio
    Q2 = RD ' Columna en la galaxia, aleatorio
    S1 = RD ' Fila en el cuadrante, aleatorio
    S2 = RD ' Columna en el cuadrante, aleatorio
    F1 = .86
    F2 = .01
    F3 = .95
    F4 = .99

    Do
        KL = 0 ' Total de Krinngs en la galaxia
        BL = 0 ' Total de bases en la galaxia
        For I = 1 To 7 ' Recorre todas las filas de la galaxia
            For J = 1 To 7 ' Recorre todas las columnas de la galaxia
                R1 = Rnd ' Factor aleatorio usado para cálculo de krinngs en el cuadrante
                R2 = Rnd ' Factor aleatorio para cálculo de bases en el cuadrante
                ' Álgebra de boole para agregar hasta 3 krinngs en el cuadrante
                KS = -(R1 > F1 - F2 * RF) - (R1 > F3 - F2 * RF) - (R1 > F4 - F2 * RF)
                KL = KL + KS
                Ba = -(R2 > F3)
                BL = BL + Ba
                ' Guarda en el cuadrante krinngs x 100 + bases x 10 + estrellas (1 a 7)
                GL(I, J) = 100 * KS + 10 * Ba + RD
                If Ba <> 0 Then GL(I, J) = GL(I, J) + .5
            Next J
        Next I
    Loop While (BL < 1 Or KL < 1) ' Si no hay bases o krinngs repite los cálculos

    U = KL ' Guarda total de krinngs
    Bases = BL ' Guarda total de bases
    ' Calcula tiempo disponible en función de enemigos y dificultad
    SD = KL + Rnd * KL / RF + 10 - RF

    ' Inicializa cuadrante actual
    S3 = 0
    Ba = S3
    KS = Ba
    For I = 1 To 7
        For J = 1 To 7
            SC(I, J) = 0 
    Next J, I

    ' Inicializa el buffer de enemigos para el cuadrante
    For I = 1 To 3
        For J = 1 To 3
            K(I, J) = 0
    Next J, I

    SC(S1, S2) = 1 ' Posicion de nuestra nave en el cuadrante

    XG = .01 * GL(Q1, Q2)
    KS = Int(XG) ' Krinngs en el cuadrante
    YG = (XG - KS) * 10
    Ba = Int(YG) ' Bases en el cuadrante
    S3 = GL(Q1, Q2) - 100 * KS - 10 * Ba ' Estrellas en el cuadrante
    Flag = -(KS > 0) ' Si hay enemigos poner flag a 1

    If Flag = 1 Then ' Si hay enemigos entonces...
        Red = 1
        For I = 1 To KS
            Do
                R1 = RD
                R2 = RD
            Loop While SC(R1, R2) <> 0 ' Si hay algo en esas coordenadas elegir otras
            SC(R1, R2) = 3 ' Ubica krinng en el cuadrante
            ' Buffer de enemigos. Fila, columna y escudos
            K(I, 1) = R1
            K(I, 2) = R2
            K(I, 3) = 200
        Next I
    End If

    ' Coloca bases en el cuadrante
    If Ba <> 0 Then
        Do
            R1 = RD
            R2 = RD
        Loop While (SC(R1, R2) <> 0) ' Si hay algo en esas coordenadas elegir otras
        SC(R1, R2) = 4
    End If

    ' Colocar estrellas en el cuadrante
    If S3 <> 0 Then
        For I = 1 To S3
            Do
                R1 = RD
                R2 = RD
            Loop While (SC(R1, R2) <> 0) ' Si hay algo en esas coordenadas elegir otras
            SC(R1, R2) = 2
        Next I
    End If
    GL(Q1, Q2) = Int(GL(Q1, Q2)) + .5

    ' Cargar nombres cortos de componentes e inicializar nivel de daños
    Restore textos:
    For a = 1 To 9
        Read D$(a)
        D(a) = 0
    Next
    ShowStart

    textos:
    Data RADAR CORTO,RADAR LARGO
    Data TORPEDOS,FASERS,ESCUDOS,UNIDAD WARP
    Data COMPUTADORA,REPORTES,REPARACION

End Sub


' --------------------------------------------------------------------------------
' -- Mostrar posicion en pantalla
' --------------------------------------------------------------------------------
' Escribe la posición en un diálogo existente y abierto
Sub ShowPos
    Shared Q1, Q2, S1, S2, D(), D$(), KS, SH, mainfont&, E

    _Font mainfont&
    PutLine 10, 7, 255, 255, 255, "_____________________________"
    PutTxt 9, "CUADRANTE (" + Str$(Q2) + "," + Str$(Q1) + ")"
    PutTxt 10, "SECTOR (" + Str$(S2) + "," + Str$(S1) + ")"

    ' Alerta baja de escudos
    If SH <= 200 * KS Then
        PutLine 140, 8, 255, 0, 0, "BAJA ENERGIA EN ESCUDOS"
    End If

    ' Si la unidad warp está dañada alerta
    If D(6) < 0 Then
        PutLine 10, 11, 255, 0, 0, "NO HAY " + D$(6)
    End If
End Sub

' --------------------------------------------------------------------------------
' -- Radar de largo alcance
' --------------------------------------------------------------------------------
Sub DoRla
    Shared Q1, Q2, GL(), midfont&, mainfont&, HDR$
    ' Si el radar está averiado muestra mensaje y salr
    If D(2) < 0 Then
        HDR$ = "AVERIA"
        ShowHelp "EL RADAR DE LARGO ALCANCE ESTA AVERIADO"
        WaitClick
        ClearScr 1
        Exit Sub
    End If

    ' Pone pantalla sólo con fondo de estrellas y dibuja una grilla de 3 x 3
    ClearScr 3
    DoRlaGrid
    _Font midfont&

    Y = 1
    ' Recorre la galaxia desde Q1 -1 hasta Q1 + 1 en el vertical...
    For A = (Q1 - 1) To (Q1 + 1)
        Y = Y + 7
        X = 0
        ' ... y desde Q2 - 1 a Q2 + 1 en el horizontal
        For B = (Q2 - 1) To (Q2 + 1)             
            ' Si las coordenadas quedan fuera de la galaxia se indica con ---           
            If A < 1 Or B < 1 Or A > 7 Or B > 7 Then
                ST$ = "---"
            Else
                ' Pone en ST$ los enemigos, bases y estrellas del cuadrante
                ST$ = "000" + Right$(Str$(Int(GL(A, B))), Len(Str$(Int(GL(A, B)))) - 1)
                ST$ = Mid$(ST$, Len(ST$) - 2, 3)
                ' Actualiza la carta estelar para indicar que el cuadrante fue escaneado
                GL(A, B) = Int(GL(A, B)) + .5
            End If
            ' Escribe en pantalla el valor de ST$
            PutLine 105 + X * 107, Y, 255, 255, 255, ST$
            X = X + 1
        Next B
    Next A

    _Font mainfont&
    PutLine 100, 34, 255, 255, 255, "<KRINNGS><BASES><ESTRELLAS>"
    PutLine 110, 35, 255, 255, 255, "--- SECTOR FUERA DE MAPAS"
    PutLine 80, 37, 255, 255, 255, "El R.L.A.  muestra  un  cuadrante  alrededor"
    PutLine 80, 38, 255, 255, 255, "de la ubicacion actual"

    _Display
    DoPos 1
End Sub

' --------------------------------------------------------------------------------
' -- Lanzar torpedos
' --------------------------------------------------------------------------------
Sub Torpedoes
    Shared gfxcirc&, dlgtorpedoes&, mainfont&, TP, TU, SD, HDR$
    Shared E, S1, S2, S3, S4, Q1, Q2, A1, B1, CS, KS, I, Target, Vista

    ' Prepara un arreglo para guardar los vectores 
    ' (ángulo y módulo relativos a nuestra nave) a los enemigos
    Dim Enemies(3) As Enemy

    ' Si están averiados muestra mensaje y sale
    If D(3) < 0 Then
        HDR$ = "AVERIA"
        ShowHelp "EL SISTEMA DE TORPEDOS ESTA AVERIADO"
        WaitClick
        ClearScr 2
        Exit Sub
    End If

    ' Calcula angulo (deg) y distancia relativos de los enemigos
    If KS > 0 Then
        For I = 1 To 3
            Enemies(I).m = DT ' Calcula el módulo
            Enemies(I).an = Atn((K(I, 1) - S1) / (K(I, 2) - S2)) ' Y el ángulo
            a = P2(Enemies(I).an * 180 / Pi) - 90
            ' Convierte el ángulo a sexagesimal y se asegura que quede en el rango 0 - 360
            If a < 0 Then a = a + 360
        Next I
    End If

    SaveScr 2
    Mask
    _Font mainfont&

    ' Si ni hay enemigos muestra mensaje y sale
    If KS = 0 Then
        HDR$ = "TORPEDOS"
        ShowHelp "NO HAY NAVES ENEMIGAS EN EL CUADRANTE"
        WaitClick
        ClearScr 2
        Exit Sub
    End If

    ' Si no quedan torpedos avisa y sale
    If TP = 0 Then
        HDR$ = "TORPEDOS"
        ShowHelp "NO QUEDAN TORPEDOS A BORDO"
        WaitClick
        ClearScr 2
        Exit Sub
    Else
        ' En caso contrario, muestra cuantos quedan en la vista de torpedos
        _PutImage (0, 7), dlgtorpedoes&
        PutLine 10, 2, 255, 255, 255, "TORPEDOS"

        TU = TU + .05

        PutLine 200, 8, 255, 255, 255, "QUEDAN" + Str$(TP)
        PutLine 200, 9, 255, 255, 255, "TORPEDOS DE PROTONES"
    End If

    ' Toma la dirección de disparo de 1 a 12 siguiendo las horas del reloj
    an = 0
    salida = 0
    Do
        _PutImage (18, 30), gfxcirc&
        DrawArrow (an * 30)
        _Limit 30
        _Display

        a = 0

        Do While _MouseInput
        Loop
        If _MouseButton(1) Then
            X = _MouseX
            Y = _MouseY

            If (Y > 137) And (Y < 161) Then
                If (X > (276) And X < (330)) Then a = 4
                If (X > (351) And X < (400)) Then a = 3
            Else If (X > 145) And (X < 180) Then
                    If (Y > 35) And (Y < 83) Then a = 1
                    If (Y > 90) And (Y < 138) Then a = 2
                End If
            End If

            Do
                I = _MouseInput
            Loop Until Not _MouseButton(1)
        End If

        k$ = InKey$
        If Len(k$) > 0 Then
            Select Case k$
                Case Is = Chr$(0) + "H"
                    a = 1
                Case Is = Chr$(0) + "P"
                    a = 2
                Case Is = Chr$(27)
                    a = 3
                Case Is = Chr$(13)
                    a = 4
            End Select
        End If

        If a > 0 Then
            Select Case a
                Case Is = 1
                    an = an + 1
                    If an = 12 Then an = 0
                Case Is = 2
                    an = an - 1
                    If an = -12 Then an = 0
                Case Is = 3
                    salida = 1
                Case Is = 4
                    salida = 1
            End Select
        End If

    Loop While salida = 0

    ' Si se canceló o no quedan torpedos entonces salir
    If a = 3 Or TP = 0 Then Exit Sub

    ' Ajusta el ángulo para que esté entre 0 y 12
    If an < 0 Then an = 12 + an
    ClearScr 2

    j = 1
    nan = an * 30 ' Guarda el ángulo en sexagesimal

    ' Selección de objetivo
    For I = 1 To KS ' Analiza a todos los enemigos en el cuadrante
        E = CInt(Enemies(I).an * 180 / Pi) ' Ángulo al enemigo en sexagesimal
        dx = K(I, 2) - S2 
        dy = K(I, 1) - S1

        r = 0 ' Ángulo resultante 
        If dy < 0 Then ' Si el enemigo está arriba de nuestra nave
            If dx = 0 Then ' Si está alineado justo arriba
                r = 270
            Else
                If dx > 0 Then ' Si está a la derecha
                    r = 360 - Abs(E)
                Else ' si está a la izquierda
                    r = 270 - (90 - E)
                End If
            End If
        Else ' No está arriba
            If dy > 0 Then ' Si está abajo
                If dx = 0 Then ' Alineado debajo
                    r = -1
                Else
                    If dx > 0 Then ' A la derecha
                        r = E
                    Else ' A la izquierda
                        r = 180 + E
                    End If
                End If
            End If
        End If

        If r = 0 Then ' Aún no se asignó ánglo
            If dx < 0 Then ' Está a la izquierda
                If dy = 0 Then ' Alineado justo a la izquierda
                    r = 180
                Else
                    If dy > 0 Then ' Debajo
                        r = 180 + E
                    Else ' Arriba
                        r = 180 + E
                    End If
                End If
            Else ' No está a la izquierda, está a la derecha
                If dy = 0 Then ' Alineado a la derecha
                    r = 90
                Else
                    If dy > 0 Then ' Derecha abajo
                        r = (90 + E) + 90
                    Else ' Derecha arriba
                        r = 180 + E
                    End If
                End If

            End If
        End If

        ' Si el ángulo es > de 0 y no es 90 compensa la ubicación del 0
        ' que para qbasic está a la derecha y para el resto de los
        ' mortales está arriba
        If r > 0 And r <> 90 Then
            r = r + 90
            If r < 0 Then r = 360 - r
            If r > 359 Then r = r - 360
        End If
        If r = -1 Then r = 180

        ' Guarda el ángulo sexagesimal
        Enemies(I).an = r
        If Enemies(j).an >= nan Then d1 = Enemies(j).an - nan Else d1 = nan - Enemies(j).an
        ' En d1 calcula la diferencia  entre el ángulo selecionado y el del enemigo más próximo
        ' en d2 va la diferencia entre el enemigo actual y el ángulo seleccionado
        If r > nan Then d2 = r - nan Else d2 = nan - r
        ' Si el enemigo actual está más cerca del ángulo seleccionado, seleccionarlo
        If d2 < d1 Then j = I
    Next
    Target = j 

    HDR$ = "TORPEDOS" 
    If D(7) >= 0 Then ' Si la computadora funciona calcula solución

        ' Si la distancia angular entre el enemigo seleccionando y el ángulo
        ' indicado es mayor de 30º se asume que no may enemigos en esa dirección, alerta y sale
        If Abs(Enemies(Target).an - nan) > 30 Then
            ShowHelp "NO HAY ENEMIGOS EN ESA DIRECCION:"
            WaitClick
            ClearScr 2
            Exit Sub
        End If

        ' Guarda la vista en el buffer 5, si está habilitada la vista
        ' (Debajo está el RCA), dibuja un círculo rojo en la posición a atacar
        ' Muestra mensaje indicando ubicación del objetivo
        SaveScr 5: _Display
        If Vista = 1 Then Circle ((K(Target, 2) - 1) * 64 + 38, (K(Target, 1) - 1) * 64 + 39), 30, _RGB(255, 0, 0)
        ShowHelp "PREPARANDO SOLUCION PARA ENEMIGO EN (" + Str$(K(Target, 2)) + "," + Str$(K(Target, 1)) + ")"
        WaitClick
        ClearScr 5
    Else ' Si no avisa y lanza el torpedo en el ángulo indicado
        ShowHelp "COMPUTADORA AVERIADA, NO SE PUEDE CALCULAR SOLUCION DE TORPEDO"
        WaitClick
        ClearScr 5
      Enemies(1).an = nan
        Enemies(1).m = 15
        Target = 1
    End If

    TU = TU + 0.05

    ' Si se agotó el tiempo dar Game Over por tiempo
    If TU > SD Then
        GameOver 3
        Exit Sub
    Else
        TP = TP - 1 ' Un torpedo menos
        ' Comienza disparo
        ds = Enemies(Target).an - 90
        ' Se asegura que el ángulo esté entre 0 y 359
        While ds >= 360: ds = ds - 360: Wend
        _Limit 3
        For w = 1 To Enemies(Target).m ' recorre w sectores hasta el enemigo seleccionado
            wy2 = Ceil(S1 + w * Sin(ds * CX))
            wx2 = Ceil(S2 + w * Cos(ds * CX))
            If wy2 <> wy Or wx2 <> wx Then ' Si estamos en un sector diferente
                If wy2 > 0 And wy2 < 8 And wx2 > 0 And wx2 < 8 Then ' Aún dentro del cuadrante
                    If SC(wy2, wx2) >= 2 Then ' Si hay algo y no es nuestra nave
                        S3 = wy2: S4 = wx2
                        If (SC(wy2, wx2) = 3) And (Vista = 1) Then
                            Boom wx2, wy2 ' Si es una nave enemiga animar explosión
                            Circle ((wx2 - 1) * 64 + 38, (wy2 - 1) * 64 + 39), 30, _RGB(255, 200, 0)
                        End If
                        TorpedoHit ' Control de colisiones
                        If E > 0 Then ' Si hubo colisión pasar turno a enemigos y salir de rutina
                            KrinngAttack
                            Exit Sub
                        End If
                    End If
                    ' Si estabas en el RCA y no hay nada en el cuadrante marcarlo
                    ' con un círculo amarillo
                    If (Vista = 1) And (SC(wy2, wx2) = 0) Then
                        Circle ((wx2 - 1) * 64 + 38, (wy2 - 1) * 64 + 39), 10, _RGB(255, 255, 0)
                        PSet ((wx2 - 1) * 64 + 38, (wy2 - 1) * 64 + 39), _RGB(0, 0, 0)
                        Paint ((wx2 - 1) * 64 + 38, (wy2 - 1) * 64 + 39), _RGB(255, 255, 0)
                        _Display
                        _Limit 3
                    End If
                    wy = wy2
                    wx = wx2
                End If
            End If
        Next

        ' Llegó a destino sin impactar? Explotar torpedo afectando lo que esté
        ' un sector a la redonda
        For ty = wy - 1 To wy + 1
            For tx = wx - 1 To wx + 1
                If tx > 0 And tx < 8 And tx > 0 And ty < 8 Then
                    S3 = ty: S4 = tx
                    E = 99
                    If (SC(S3, S4) = 3) And (Vista = 1) Then Boom tx, ty
                    TorpedoHit
                End If
        Next tx, ty
    End If
    KrinngAttack ' Pasar turno a enemigos antes de salir
End Sub

' --------------------------------------------------------------------------------
' -- Deteccion de colision
' --------------------------------------------------------------------------------
Sub TorpedoHit
    Shared KS, KL, K(), BL, SC(), GL(), RK
    Shared E, S3, S4, Q1, Q2, HDR$

    ' S4,S3:_ Coordenadas de nuestra nave

    HDR$ = "ATAQUE"
    Silent = E = 99
    E = 0

    Select Case SC(S3, S4) + 1
        Case Is = 1, 2
            Exit Sub
        Case Is = 3
            If Silent = 0 Then
                ShowHelp "NO PUEDES DESTRUIR ESTRELLAS"
                WaitClick
                ClearScr 2
            End If
            E = 3
            Exit Sub
        Case Is = 4
            If Rnd < (.04 * RK) Then
                ShowHelp "LOS ESCUDOS KRINNG RECHAZARON EL TORPEDO"
                WaitClick
                ClearScr 2
                E = 3
                Exit Sub
            Else
                ShowHelp "KRINNG EN(" + Str$(S4) + "," + Str$(S3) + ") DESTRUIDO"
                WaitClick
                ClearScr 2
                E = 3
                SC(S3, S4) = 0
                GL(Q1, Q2) = GL(Q1, Q2) - 100
                KS = KS - 1
                KL = KL - 1
                If KL = 0 Then
                    GameOver 4
                    E = 4
                    Exit Sub
                End If

                ' Crea un arreglo vacío de 3 x 3                
                Dim f(3, 3)
                I = 1
                For a = 1 To 3
                    f(a, 1) = 0: f(a, 2) = 0: f(a, 3) = 0 ' Inicializa el valor
                    If (K(a, 1) = S3) And (K(a, 2) = S4) Then ' Hay enemigo en el sector
                        ' Lo elimina
                        K(a, 1) = 0
                        K(a, 2) = 0
                        K(a, 3) = 0
                    Else ' El enemigo está en otro lado
                        If K(a, 1) > 0 And K(a, 2) > 0 Then ' No fue eliminado
                            ' Agregarlo al arreglo
                            f(I, 1) = K(a, 1)
                            f(I, 2) = K(a, 2)
                            f(I, 3) = K(a, 3)
                            I = I + 1
                        End If
                    End If
                Next a
                ' Mover los valores del arreglo nuevo al de enemigos en el cuadrante KS()
                For a = 1 To 3: For B = 1 To 3: K(a, B) = f(a, B): Next B, a
                Exit Sub
            End If
        Case Is = 5
            ShowHelp "BASE DESTRUIDA"
            WaitClick
            ClearScr 2

            BA = 0
            BL = BL - 1
            SC(S3, S4) = 0
            GL(Q1, Q2) = GL(Q1, Q2) - 10
            E = 5
            Exit Sub
    End Select
End Sub

' --------------------------------------------------------------------------------
' -- Mostrar mensaje de game over
' --------------------------------------------------------------------------------
Sub GameOver (Opc)
    Shared titlefont&, SH, TU, U, KL, YG, BL, En, SH, RF, HDR$, over&, bgm&, Bases

    ClearScr 1
    Line (0, 0)-(639, 479), _RGBA(0, 0, 64, 96), BF
    Line (0, 100)-(639, 230), _RGBA(0, 0, 0, 225), BF
    _Font titlefont&
    Color _RGB(255, 255, 255), 0
    Locate 4, 100
    Print "GAME OVER"

    HDR$ = "GAME OVER"
    MSG$ = ""

    s = 0

    Select Case Opc
        Case Is = 2
            MSG$ = "NAVE IMPOSIBILITADA POR ATAQUE KRINNG\\QUEDAN" + Str$(KL) + " NAVES KRINNG"
            s = 1
        Case Is = 3
            MSG$ = "MUERTE EN EL ESPACIO\\QUEDAN" + Str$(KL) + " NAVES KRINNG\TIEMPO RESTANTE: " + Str$(P2(TU))
        Case Is = 4
            MSG$ = "LA FEDERACION SE SALVO"
            s = 1
        Case Is = 5
            MSG$ = "TIEMPO AGOTADO, LOS KRINNG DESTRUYERON LA FEDERACION\QUEDAN" + Str$(KL) + " NAVES KRINNG\\FALLASTE!"
            s = 1
    End Select

    ' Si corresponde calcula puntuación considerando los enemigos destruídos (U - KL),
    ' las bases aliadas remanentes(Bases - BL), el tiempo usado (TU), la energía que queda en
    ' la nave (En + SH) y por supuesto la dificultad (RF)
    If s = 1 Then
        L = (U - KL) * 10 + ((U - KL) * 500 / TU) - 100 * (Bases - BL)
        If (En + SH) <> 0 Then L = L - 200
        If KL = 0 Then L = L + (RF * 100)

    ' Prepara un mensaje de resúmen de juego terminado
        MSG$ = MSG$ + "\QUEDAN " + Str$(BL) + " BASES (" + Str$(Bases - BL) + " perdidas)\" + LTrim$(Str$(U - KL)) + " ENEMIGOS ABATIDOS\RATING:" + Str$(P2(L))

        Select Case L
            Case Is < -100
                A$ = "UNA BAJA DESHONROSA"
            Case Is < 0
                A$ = "SER DEGRADADO"
            Case Is < 100
                A$ = "UN ASCENSO"
            Case Is < 200
                A$ = "ASCENSO A ALMIRANTE"
            Case Is < 300
                A$ = "NOMBRAMIENTO DE GENERAL"
            Case Is < 400
                A$ = "SER LEYENDA"
            Case Is >= 400
                A$ = "SER COMO UN DIOS VIVIENTE, GRACIAS"
        End Select

        MSG$ = MSG$ + "\\HAS LOGRADO: " + A$
    End If

    ' Muestra el mensaje
    MSG$ = MSG$ + "\\Pulsa una tecla o haz clic para volver a empezar"
    ShowHelp MSG$
    _Display

    ' Detiene la música y suena voz de Game over
    _SndStop bgm&
    _SndPlay over&

    While _SndPlaying(over&): Wend

    ' Espera clic / tecla y reinicia el juego
    WaitClick
    Run
End Sub

' --------------------------------------------------------------------------------
' -- Mostrar resumen de enemigos restantes
' --------------------------------------------------------------------------------
Sub krinngs (Ry)
    Shared KL

    Color _RGB(255, 255, 255), 0
    If Ry <> 12 Then
        PutTxt Ry, "QUEDAN" + Str$(KL) + " NAVES KRINNG"
    Else
        PutLine 240, 4, 255, 255, 255, "KRINNGS: " + Str$(KL)
    End If
End Sub

' --------------------------------------------------------------------------------
' -- Atacan los enemigos, turno de la IA
' --------------------------------------------------------------------------------
Sub KrinngAttack
    Shared KS, C$, K(), D(), D$(), SC(), E, h, HDR$, S1, S2, SH, RF

    SaveScr 6

    If KS = 0 Then Exit Sub
    HDR$ = "KRINNG"
    MSG$ = "NAVE KRINNG ATACA"
    ' Si estás anclado se muestra mensaje y aborta ataque
    If C$ = "D" Then
        MSG$ = MSG$ + ", LA BASE NOS PROTEGE"
        ShowHelp MSG$
        WaitClick
        ClearScr 6
        Exit Sub
    Else

        MSG$ = ""
        E = 0
        For I = 1 To 3 ' Recorrer el arreglo de enemigos en el cuadrante
            If E = 0 And K(I, 3) > 0 Then ' Si no hubo ataque y este no fue eliminado entonces...
                ' Calcula el ataque enemigo en base a su energía y un factor aleatorio
                h = K(I, 3) / DT + Sgn(Rnd - .5) * Rnd * 7
                SH = SH - h
                ' Prepara mensaje con resumen de ataque
                If MSG$ <> "" Then MSG$ = MSG$ + "\"
                MSG$ = MSG$ + Str$(P2(h)) + " PUNTOS DE ATAQUE DESDE (" + Str$(K(I, 2)) + "," + Str$(K(I, 1)) + ")"
                MSG$ = MSG$ + "\DEJA " + Str$(P2(SH)) + " DE ESCUDOS"
                If SH < 0 Then GameOver 2 ' Si nos dejó sin escudos es Game over

                ' Usa la dificultad y un factor aleatorio para determinar si nos averiaron algo
                If Rnd <= (RF / 23) Then
                    r1 = Int(Rnd * 9 + 1) ' Número aleatorio de 1 a 8
                    If D(r1) >= 0 Then ' Si el componente no está dañado romperlo
                        D(r1) = D(r1) - 10 * Rnd - 1
                        If D(8) >= 0 Then
                            HDR$ = "ROTURA" ' Prepara aviso
                            If MSG$ <> "" Then MSG$ = MSG$ + "\"
                            MSG$ = MSG$ + D$(r1) + " AVERIADO"
                        End If
                    End If
                End If

                ' Después de atacar la nave se mueve
                If Rnd <= RF / 15 Then
                    r1 = RD
                    r2 = RD

                    ' Marca un sector al azar donde no puede ir nadie
                    If SC(r1, r2) = 0 Then SC(r1, r2) = -1
                    E = 2
                    While SC(r1, r2) <> 0
                        r1 = RD
                        r2 = RD
                    Wend
                    SC(r1, r2) = 3
                    SC(K(I, 1), K(I, 2)) = 0
                    K(I, 1) = r1
                    K(I, 2) = r2
                End If
            End If
            If E > 0 Then E = E - 1
        Next I

        ' Si hay un mensaje, mostrarlo, esperar clic / tecla, limpiar pantalla y salir
        If MSG$ <> "" Then
            ShowHelp MSG$
            WaitClick
            ClearScr 6
        End If
        Exit Sub

    End If

End Sub

' --------------------------------------------------------------------------------
' -- Controlar cuánta energía se asigna a escudos
' --------------------------------------------------------------------------------
Sub Shields
    Shared dlgslider&, mainfont&, midfont&, E, En, SH, HDR$, D()

    SaveScr 2
    Mask

    ' Si el control de escudos está averiado muestra mensaje y sale
    If D(5) < 0 Then
        HDR$ = "AVERIA"
        ShowHelp "LOS ESCUDOS ESTAN AVERIADOS"
        WaitClick
        ClearScr 2
        Exit Sub
    End If

    ' Prepara y muestra el control de energía a escudos
    _Font midfont&
    _PutImage (0, 7), dlgslider&

    PutLine 30, 5, 255, 255, 255, Str$(Floor(En + SH))
    PutLine 20, 4, 255, 255, 255, "ENERGIA:"
    PutLine 20, 7, 255, 255, 255, "ESCUDOS:"

    _Font mainfont&
    PutLine 50, 12, 255, 255, 255, Str$(Floor(SH))

    PutTxt 2, "ESCUDOS"

    PutLine 200, 5, 255, 255, 255, "TRANSFERIR ENERGIA A"
    PutLine 200, 6, 255, 255, 255, "ESCUDOS, < MAX:" + Str$(Floor(En + SH)) + ">"
    PutLine 200, 8, 255, 255, 0, "?> " + Str$(Floor(SH)) + "_"
    _Display
    a = UpdNumeric(Floor(En + SH), Floor(SH))
    V = SH
    E = 0
    salida = 0
    Do
        _Limit 30

        V = ProcessNumeric(En + SH, V)
        If E = 10 And En + SH - V < 0 Then V = 0: E = 0
        Line (200, 83)-(322, 98), _RGB(0, 0, 0), BF

        Locate 8, 200
        PutLine 200, 8, 255, 255, 0, "?> " + Str$(V) + "_"
        _Display

        If E = 10 Or E = 11 Then salida = 1
    Loop While salida = 0

    ClearScr 2

    If E = 11 Then Exit Sub ' Si se cancela, salir
    ' Si no, ajusta escudos y reserva de energía
    En = En + SH - V
    SH = V
End Sub

' --------------------------------------------------------------------------------
' -- Disparar phasers
' --------------------------------------------------------------------------------
Sub Phasers
    Shared mainfont&, midfont&, dlgslider&, KS, D(), En, TU, K(), E, HDR$, GL(), SC(), Q1, Q2, KL

    SaveScr 2
    Mask

    ' Si los phaser están dañados muestra mensaje y sale
    If D(4) < 0 Then
        HDR$ = "AVERIA"
        ShowHelp "EL SISTEMA DE PHASERS ESTA AVERIADO"
        WaitClick
        ClearScr 2
        Exit Sub
    End If

    ' Si no hay enemigos en el cuadrante muestra mensaje y sale
    If KS <= 0 Then
        HDR$ = "ERROR"
        ShowHelp "NO SE DETECTAN KRINNGS EN EL RADAR\DE CORTO ALCANCE"
        WaitClick
        ClearScr 2
        Exit Sub
    End If

    ' Si la computadora está dañada advierte de errores de cálculo
    If D(7) < 0 Then
        HDR$ = "ALERTA"
        ShowHelp "LA COMPUTADORA FALLA EN LA\CANTIDAD DE AMPERES"
        WaitClick
        ClearScr 2
    End If

    _PutImage (0, 7), dlgslider&

    _Font midfont&

    PutLine 30, 5, 255, 255, 255, Str$(Floor(En))
    PutLine 20, 4, 255, 255, 255, "ENERGIA:"
    PutLine 20, 7, 255, 255, 255, "PHASERS:"

    _Font mainfont&
    PutTxt 2, "PHASERS"

    PutLine 50, 12, 255, 255, 255, "0"
    PutLine 200, 5, 255, 255, 255, "TRANSFERIR ENERGIA A"
    PutLine 200, 6, 255, 255, 255, "PHASERS, < MAX:" + Str$(Floor(En)) + ">"
    PutLine 200, 8, 255, 255, 255, "?> _"
    PutLine 200, 10, 255, 255, 255, "PHASERS BLOQUEADOS"

    _Display

    ' Toma valor de energía a usar para atacar
    E = 0
    V = 0
    Do
        _Limit 30

        V = ProcessNumeric(En, V)
        Line (200, 83)-(322, 98), _RGB(0, 0, 0), BF
        PutLine 200, 8, 255, 255, 255, "?> " + Str$(V) + "_"
        _Display
    Loop While E = 0

    ' Si se cancela, cerrar y salir
    If E = 11 Then
        ClearScr 2
        Exit Sub
    End If

    ' Incrementa tiempo usado, si nos pasamos del límite es Game over
    TU = TU + .05
    If TU > SD Then
        GameOver 5
    End If

    ' Baja la energía de la reserva y si no estamos anclados deja que los krinng ataquen
    En = En - V
    If C$ <> "D" Then
        KrinngAttack
    End If

    If D(7) < 0 Then V = V * Rnd ' Si la computadora está dañada alterar magnitud de ataque (reducción)

    msg$ = "DISPARANDO PHASERS"

    ' Hacer el ataque
    Dim f(3, 3)
    k = 1
    S = 0
    For I = 1 To 3 ' Recorre arreglo de ennemigos en cuadrante
        If K(I, 3) > 0 Then ' Si existe nave entonces...
            ' Calcula impacto considerando distancia, energía usada y factor aleatorio
            H = (V / DT) + Sgn(Rnd - .5) * 7 * Rnd 
            K(I, 3) = K(I, 3) - H
            If K(I, 3) < 0 Then K(I, 3) = 0 ' Su energía no puede ser menor a 0
            S = 1
            f(I, 1) = 0: f(I, 2) = 0: f(I, 3) = 0 ' Inicializa posición del buffer de intercambio
            ' Prepara mensaje de resultado de ataque
            msg$ = msg$ + "\ IMPACTO DE " + Str$(P2(H)) + " EN (" + Str$(K(I, 2)) + "," + Str$(K(I, 1)) + "), QUEDA EN " + Str$(P2(K(I, 3)))
            If K(I, 3) <= 0 Then ' Si fue destruído
                msg$ = msg$ + ", DESTRUIDO"
                ' Ajusta los conteos de enemigos en el cuadrante y en la galaxia
                KS = KS - 1
                KL = KL - 1
                If KL = 0 Then GameOver 4 ' Si los mataste a todos ganaste
                ' Ajusta la carta estelar y elimina al enemigo del arreglo
                GL(Q1, Q2) = GL(Q1, Q2) - 100
                SC(K(I, 1), K(I, 2)) = 0
                K(I, 1) = 0
                K(I, 2) = 0
                K(I, 3) = 0
            Else ' si no fue destruído es agregado al arreglo de intercammbio
                f(k, 1) = K(I, 1)
                f(k, 2) = K(I, 2)
                f(k, 3) = K(I, 3)
                k = k + 1
            End If
        End If
    Next I

    ' Vuelca el arreglo de intercambio en el arreglo de enemigos en el cuadrante
    For I = 1 To 3
        For j = 1 To 3
            K(I, j) = f(I, j)
    Next j, I

    ' Si hay un mensaje mostrarlo, esperar clic / tecla y limpiar pantalla
    If S = 1 Then
        ShowHelp msg$
        WaitClick
        ClearScr 2
    End If
End Sub

' --------------------------------------------------------------------------------
' -- Unidad Warp
' --------------------------------------------------------------------------------
Function WarpDrive
    Shared D(), WP, TC, dlgcourse&, gfxcirc&, mainfont&, E, MAXSP, KS, En, C$
    Shared Q1, Q2, S1, S2, TU, SD, SC()
    Shared A1, B1, CS, HDR$

    ' Guarda pantalla y pone mascara para ocultar controles principales
    SaveScr 2
    Mask
    ' Si la UW está dañada muestra mensaje y sale
    If D(6) < 0 Then
        HDR$ = "AVERIA"
        ShowHelp "LA UNIDAD WARP ESTA AVERIADA"
        WaitClick
        ClearScr 2
        Exit Function
    End If

    _PutImage (0, 7), dlgcourse&
    _Display

    ' Inicializa valores y toma ángulo y módulo del salto
    an = 0
    v = 1
    f = 0
    salida = 0
    E = 0
    HDR$ = "UNIDAD WARP"

    _Font mainfont&
    PutLine 10, 2, 255, 255, 255, "UNIDAD WARP"
    _Font mainfont&
    Color _RGB(255, 0, 255)
    Locate 13, 190
    Print "MAX:"; MAXSP

    UpdWF v, f

    Do
        _PutImage (18, 30), gfxcirc&
        DrawArrow (an * 30)
        _Limit 30
        _Display

        a = 0

        Do While _MouseInput
        Loop
        If _MouseButton(1) Then
            X = _MouseX
            Y = _MouseY

            If (Y > 137) And (Y < 161) Then
                If (X > (276) And X < (330)) Then a = 3
                If (X > (351) And X < (400)) Then a = 4
            Else If (X > 145) And (X < 180) Then
                    If (Y > 35) And (Y < 83) Then a = 7
                    If (Y > 90) And (Y < 138) Then a = 8
                End If
            End If

            If (Y > 99 And Y < 121) Then
                If (X > 216 And X < 246) Then a = 2
                If (X > 251 And X < 282) Then a = 1
                If (X > 290 And X < 322) Then a = 5
                If (X > 325 And X < 357) Then a = 6
            End If
            Do
                I = _MouseInput
            Loop Until Not _MouseButton(1)
        End If

        k$ = InKey$
        k$ = UCase$(k$)
        If Len(k$) > 0 Then
            Select Case k$
                Case Is = "X"
                    a = 1
                Case Is = "Z"
                    a = 2
                Case Is = Chr$(27)
                    a = 4
                Case Is = Chr$(13)
                    a = 3
                Case Is = "C"
                    a = 5
                Case Is = "V"
                    a = 6
                Case Is = Chr$(0) + "H"
                    a = 7
                Case Is = Chr$(0) + "P"
                    a = 8

            End Select
        End If

        If a > 0 Then
            E = 0
            Select Case a
                Case Is = 1
                    v = v + 1
                Case Is = 2
                    v = v - 1
                Case Is = 3
                    salida = 1
                    E = 11
                Case Is = 4
                    salida = 1
                    E = 10
                Case Is = 5
                    f = f - 1
                Case Is = 6
                    f = f + 1
                Case Is = 7
                    an = an + 1
                Case Is = 8
                    an = an - 1
            End Select
        2 End If

        While v < 0: v = 10 + v: Wend
        While v > 9: v = v - 10: Wend
        While f < 0: f = 10 + f: Wend
        While f > 9: f = f - 10: Wend
        While an < 1: an = 12 - an: Wend
        While an > 12: an = an - 12: Wend
        _Limit 10
        UpdWF v, f

        WP = v + (f / 10)
        _Display

        ' Si se aceptó pero la UW está dañada o el módulo (factor warp) supera 
        ' el máximo permitido muestra mensaje y cancela.
        If E = 11 And (D(6) < 0 Or WP > MAXSP) Then
            SaveScr 4
            ShowHelp "VELOCIDAD MAXIMA: " + Str$(MAXSP)

            salida = 0
            WaitClick

            ClearScr 4
            salida = 0
            E = 0
        End If

        If E = 11 Then
            SaveScr 4
            msg$ = ""
' Recorre arreglo de ennemigos en cuadrante
        If K(I, 3) > 0 Then ' Si existe nave entonces...
            ' Calcula impacto considerando distancia, energía usada y factor aleatorio
            H = (V / DT) + Sgn(Rnd - .5) * 7 * Rnd 
            K(I, 3) = K(I, 3) - H
            If K(I, 3) < 0 Then K(I, 3) = 0 ' Su energía no puede ser menor a 0
            S = 1
            f(I, 1) = 0
            If En <= 12 * WP Then ' Si no hay energía para saltar...
                If SH < 1 Then ' ni reserva en escudos es game over
                    WarpDrive = 1
                    GameOver 2
                    Exit Function
                End If
                ' ... muestra mensaje y cancela el salto
                HDR$ = "ERROR"
                ShowHelp "QUEDAN " + Str$(P2(En)) + " UNIDADES DE ENERGIA\MAX WARP: " + Str$(Int(En / 12))
                WaitClick
                ClearScr 4
                E = 0
                salida = 0
            Else ' Ajusta el factor para que el entero coincida con el número
            ' de cuadrantes a saltar y el decimal con los sectores
                If WP < 1 Then WP = WP * 1.42858
                If Floor(WP) <> WP Then
                    WP = Floor(WP) + (WP - Floor(WP) * 1.42858)
                End If
                WP = WP * 7
            End If
        End If
    Loop While salida = 0

    ClearScr 2
    ' Si canela salir de función
    If E = 10 Then
        WarpDrive = 1
        Exit Function
    End If

    ' Convertir ángulo a sexagesimal
    ds = 360 - (12 - an) * 30 - 90
    WP = Int(WP)
    While ds >= 360: ds = ds - 360: Wend

    TC = TC + 1 ' Agregar unidad de reparación
    C$ = "" ' Quitar anclajes
    En = En - 2 * WP ' Consumir energía
    TU = TU + WP / 25 ' Consumir tiempo
    If TU > SD Then GameOver 5 ' Si se  agotó el tiempo es Game over

    If KS > 0 Then KrinngAttack ' Si quedan enemigos en el cuadrante, atacan

    SaveScr 5
    msg$ = ""

    ' Avanza un poco en la reparación de componentes dañados
    For I = 1 To 9
        If D(I) < 0 Then ' Si está roto entonces...
            D(I) = D(I) + 1
            If D(I) > 0 And D(8) >= 0 Then ' Si quedó reparado y funciona
                ' el reporte de daños, mostrar mensaje  de reparación
                If msg$ <> "" Then msg$ = msg$ + "\"
                msg$ = msg$ + D$(I) + " REPARADO"
            End If
        End If
    Next I

    ' Aleatoriamente repara completamente un componente dañado
    If Rnd <= .1 Then
        R1 = RD
        If D(R1) < 0 And D(8) < 0 Then
            D(R1) = 0
            If msg$ <> "" Then msg$ = msg$ + "\"
            msg$ = msg$ + D$(R1) + " REPARADO"
        End If
    End If

    ' Si hay mensaje, lo muestra
    If msg$ <> "" Then
        HDR$ = "REPARAR"
        ShowHelp msg$
        WaitClick
        ClearScr 5
    End If

    ' Calcula sector absoluto de destino (dx, dy)
    dy1 = (S1 - 1) * 64 + 37
    dx1 = (S2 - 1) * 64 + 39
    dy = dy1 + (WP * 64) * Sin(ds * CX)
    dx = dx1 + (WP * 64) * Cos(ds * CX)

    ' Las coordenadas absolutas de origen se guardan en (ax, ay)
    ax = (Q2 - 1) * 7 + S2
    ay = (Q1 - 1) * 7 + S1
    ' Las coordenadas absolutas de la nave en el cuadrante se guardan en (wx, wy)
    wx = S2
    wy = S1
    wf = 10 ' Factor warp usado para al menos salir del cuadrante
    If WP < 10 Then wf = WP ' Si se indicó un número menor, entonces usarlo

    For w = 1 To wf ' Sectores a saltar, coordenadas absolutas del sector en (wx2, wy2)
        wy2 = Ceil(S1 + w * Sin(ds * CX))
        wx2 = Ceil(S2 + w * Cos(ds * CX))

        If wy2 <> wy Or wx2 <> wx Then ' Si el sector no se analizó entonces...
            If wy2 > 0 And wy2 < 8 And wx2 > 0 And wx2 < 8 Then ' Si estamos dentro del cuadrante entonces...
                If SC(wy2, wx2) >= 2 Then ' Hay algo en el cuadrante y no es la nave (1)
                    SC(S1, S2) = 0
                    SC(wy, wx) = 1 ' Mueve la nave al sector anterior y...
                    ' muestra un mensaje avisando
                    msg$ = "DETENIDO EN (" + Str$(wx) + "," + Str$(wy) + ")"
                    TU = TU + WP / 24 ' Calcula el tiempo usado
                    msg$ = msg$ + "\PERDISTE " + Str$(P2(WP / 24)) + " DIAS"
                    If SC(wy2, wx2) = 4 Then
                        msg$ = msg$ + "\\ANCLASTE"
                        DockShip
                    End If
                    ShowHelp msg$
                    WaitClick
                    ClearScr 5
                    WarpDrive = 0
                    S1 = wy
                    S2 = wx
                    Exit Function
                End If
                wy = wy2
                wx = wx2
            End If
        End If
    Next

    ' Calcula el sector absoluto de destino (fx, fy)
    fx = Ceil(ax + WP * Cos(ds * CX))
    fy = Ceil(ay + WP * Sin(ds * CX))

    ' Si el salto te saca de la galaxia, alerta, aborta el salto y ajusta el tiempo usado, luego sale
    _Font mainfont&
    If fx < 0 Or fx > 49 Or fy < 0 Or fy > 49 Then
        ShowHelp "PERMANECE EN LA GALAXIA"
        WaitClick
        TU = TU + WP / 24
        ClearScr 2
        WarpDrive = 0
        Exit Function
    End If

    ' Primero guarda las coordenadas iniciales
    snx% = S2
    sny% = S1
    qnx% = Q2
    qny% = Q1

    ' Si las coordenadas de destino son diferentes a las de origen entonces...
    If fx <> ((Q2 - 1) * 7 + S2) Or fy <> ((Q1 - 1) * 7 + S1) Then
        sny% = fy Mod 7
        snx% = fx Mod 7

        qnx% = (fx - snx%) / 7 + 1
        qny% = (fy - sny%) / 7 + 1

        qny% = qny% + ((fy Mod 7) = 0)
        sny% = sny% - 7 * (sny% = 0)

        qnx% = qnx% + ((fx Mod 7) = 0)
        snx% = snx% - 7 * (snx% = 0)

    End If

    ' Mueve la nave a la nueva ubicación
    SC(S1, S2) = 0
    S1 = sny%
    S2 = snx%

    ' Si cambia el cuadrante, mover enemigos
    If Q1 <> qny% Or Q2 <> qnx% Then
        Q1 = qny%
        Q2 = qnx%
        MoveKrinngs
    End If

    SC(S1, S2) = 1
    ' Busca una base alrededor de la ubicación final...
    For a = S1 - 1 To S1 + 1
        For B = S2 - 1 To S2 + 1
            If (a > 0 And a < 8) And (B < 8 And B > 0) Then
                If SC(a, B) = 4 Then ' Si encuentra una, anclarse
                    msg$ = "ANCLADO"
                    DockShip
                    ShowHelp msg$
                    WaitClick
                    ClearScr 5
                    WarpDrive = 0
                    Exit Function

                End If
            End If
        Next B
    Next a
    salida = 0
    E = 0
    WarpDrive = 0

End Function

' --------------------------------------------------------------------------------
' --  Anclar nave en base
' --------------------------------------------------------------------------------
Sub DockShip
    Shared SH, TP, C$, En, D(), RF

    En = 3100 - XA ' Energía máxima reservable
    SH = 500 + Int(RF * 125) ' 1500/12=12, es decir, en la maxima dificultad tendras 2000 de escudos
    TP = 10
    C$ = "D"
    KrinngAttack

    ' Reparación automática
    For I = 1 To 9
        If D(1) < 0 Then
            D(I) = D(I) + (11 * Rnd / RF)
            If D(I) > 0 Then D(I) = 0
        End If
    Next I

End Sub

' --------------------------------------------------------------------------------
' -- Los enemigos se mueven
' --------------------------------------------------------------------------------
Sub MoveKrinngs
    Shared KS, SC(), K(), GL(), Q1, Q2, X, Y

    S3 = 0 ' Estrellas
    BA = S3 ' Bases
    KS = BA ' Enemigos
    ' Inicializa los sectores del cuadrante actual
    For I = 1 To 7
        For J = 1 To 7
            SC(I, J) = 0
        Next J
    Next
    ' Inicializa el buffer de enemigos en el cuadrante
    For I = 1 To 3
        For J = 1 To 3
            K(I, J) = 0
        Next J
    Next I
    SC(S1, S2) = 1 ' Marca ubicación de nuestra nave en cuadrante
    X = .01 * GL(Q1, Q2)
    KS = Int(X) ' Enemigos
    Y = (X - KS) * 10
    BA = Int(Y) ' Bases
    S3 = GL(Q1, Q2) - 100 * KS - 10 * BA ' Estrellas
    If KS > 0 Then ' Si hay enemigos, los coloca en sectores vacíos
        For I = 1 To KS
            Do
                R1 = RD
                R2 = RD
            Loop While SC(R1, R2) <> 0
            SC(R1, R2) = 3
            K(I, 1) = R1
            K(I, 2) = R2
            K(I, 3) = 200
        Next I
    End If
    If BA <> 0 Then ' Si hay bases las coloca en sectores vacíos
        Do
            R1 = RD
            R2 = RD
        Loop While SC(R1, R2) <> 0
        SC(R1, R2) = 4
    End If
    If S3 <> 0 Then ' Si haay estrellas las coloca en lugares vacíos
        For I = 1 To S3
            Do
                R1 = RD
                R2 = RD
            Loop While SC(R1, R2) <> 0
            SC(R1, R2) = 2
        Next I
    End If
    GL(Q1, Q2) = Int(GL(Q1, Q2)) + .5 ' Marca el cuadrante como explorado
End Sub

' --------------------------------------------------------------------------------
' -- Mostrar carta estelar
' --------------------------------------------------------------------------------
Sub ShowChart
    Shared GL(), midfont&, mainfont&

    SaveScr 2
    ' Si la computadora está dañada no puede mostrar la carta. Avisa y sale de rutina
    If D(7) < 0 Then
        HDR$ = "AVERIA"
        ShowHelp "COMPUTADORA AVERIADA, NO HAY CARTA ESTELAR"
        WaitClick
        ClearScr 2
        Exit Sub
    End If

    ClearScr 1

    DoTitle "     CARTA ESTELAR"

    _Font midfont&
    Color _RGB(255, 255, 255), 0
    For A = 1 To 7
        For B = 1 To 7 ' Recorre el mapa galáctico interno
            Locate ((A - 1) * 4) + 3, 20 + (B - 1) * 64
            If GL(A, B) = Int(GL(A, B)) Then ' Si el cuadrante no fué explorado entonces...
                Print "ooo";
            Else ' Si fue explorado...
                ST$ = "ooo" + Right$(Str$(Int(GL(A, B))), Len(Str$(Int(GL(A, B)))) - 1)
                s$ = Mid$(ST$, Len(ST$) - 2, 3)
                Print s$;
                ' Si hay bases en el cuadrante, marcarlo en azul
                If (Mid$(s$, 2, 1) <> "o") And (Mid$(s$, 2, 1) <> "0") Then
                    Circle ((B - 1) * 64 + 38, (A - 1) * 64 + 39), 31, _RGB(64, 64, 255)
                    Circle ((B - 1) * 64 + 38, (A - 1) * 64 + 39), 28, _RGB(64, 64, 255)
                    Paint ((B - 1) * 64 + 68, (A - 1) * 64 + 39), _RGB(64, 64, 255)
                End If
            End If
            ' Si estamos en este cuadrante marcarlo en amarillo
            If (A = Q1) And (B = Q2) Then
                Circle ((B - 1) * 64 + 38, (A - 1) * 64 + 39), 26, _RGB(255, 255, 0)
                Circle ((B - 1) * 64 + 38, (A - 1) * 64 + 39), 24, _RGB(255, 255, 0)
            End If
        Next B
    Next A

    _Font mainfont&
    Locate 39, 120
    Print "<KRINNGS><BASES><ESTRELLAS>";
    _Display
    DoPos 1

End Sub

' --------------------------------------------------------------------------------
' -- Reporte de averias
' --------------------------------------------------------------------------------
Sub DamageRep
    Shared midfont&, mainfont&, D$(), D(), HDR$

    SaveScr 4

    ' Si el sistema de reportes está dañado, avisa y sale
    If D(8) < 0 Then
        HDR$ = "AVERIA"
        ShowHelp "EL REPORTE DE ROTURAS ESTA AVERIADO"
        WaitClick
        ClearScr 4
        Exit Sub
    End If

    ' Limpia la pantalla y dibuja la grilla de reportes
    BaseDamage
    _Font midfont&

    Locate 3, 100
    Print "ESTADO DE COMPONENTES"

    ' Lista todos los componentes y su estado
    _Font mainfont&
    For I = 1 To 9
        Locate 10 + I * 2, 90
        Print D$(I)
        Locate 10 + I * 2, 270
        Print P2(D(I))
    Next I

    ' Muestra breve ayuda
    Locate 35, 100
    Print "0: COMPONENTE FUNCIONAL"
    Locate 36, 100
    Print "MENOR A 0: GRADO DE ROTURA"

    _Display
End Sub

' --------------------------------------------------------------------------------
' -- Autodestruccion
' --------------------------------------------------------------------------------
Sub SelfDestruct
    Shared titlefont&, mainfont&, SC$, VK, HDR$

    SaveScr 2
    Mask

    ' Si la computadora está averiada no puede autodestruírse. Avisa y sale
    If D(7) < 0 Then
        HDR$ = "AVERIA"
        ShowHelp "COMPUTADORA AVERIADA, NO AUTODESTRUCCION"
        WaitClick
        ClearScr 2
        Exit Sub
    End If

    _Font mainfont&
    ShowOkCancel "SEGURIDAD"

    PutLine 10, 4, 255, 255, 0, "INGRESE CODIGO DE SEGURIDAD"

    If VK = 1 Then ' Si está habiilitado el teclado virtual, mostrarlo
        S$ = ShowVK$(10, 6, "?> ", 1)
        If E = 10 Then
            ClearScr 2
            Exit Sub
        End If
    Else ' Si no usae entrada de teclado físico
        _Display

        _Font mainfont&
        Locate 6, 10
        Color _RGB(255, 255, 255), 0

        Input "?> ", S$
    End If

    If S$ <> SC$ Then ' Verifica el código con el que ingresamos al inicio
        ClearScr 2
        HDR$ = "AUTODESTRUCCION"
        ShowHelp "CODIGO NO VALIDO, ABORTANDO"
        WaitClick
        ClearScr 2
        SelfDestruct
    Else ' Si valida hace secuencia de autodestrucción
        ClearScr 2
        Line (0, 0)-(639, 479), _RGBA(0, 0, 64, 96), BF
        Line (0, 200)-(639, 330), _RGBA(0, 0, 0, 225), BF
        _Font titlefont&
        Color _RGB(255, 255, 255), 0
        Locate 6, 20
        Print "DETONAR EN"
        SaveScr 4
        For I = 5 To 1 Step -1
            ClearScr 4
            Locate 6, 500
            Print I
            _Display
            Sleep 1
        Next
    End If
    GameOver 3
End Sub

' --------------------------------------------------------------------------------
' -- Reparacion de averias
' --------------------------------------------------------------------------------
Sub DamageFix
    Shared RK, TC, VK, C$, HDR$, D(), D$(), mainfont&

    Dim kv(9)

    SaveScr 4

    ' Si el sistema de reparación está dañado, muestra aviso y sale
    If D(9) < 0 Then
        HDR$ = "AVERIA"
        ShowHelp "EL SISTEMA DE REPARACION ESTA AVERIADO"
        WaitClick
        ClearScr 4
        Exit Sub
    End If

    dm = 0 ' Cantidad de módulos dañados
    For I = 1 To 9
        If D(I) < 0 Then dm = dm + 1
    Next

    ' Si no hay averías avisa y sale
    If dm = 0 Then
        MSG$ = "NO HAY AVERIAS"
        ShowHelp MSG$
        WaitClick
        ClearScr 4
        Exit Sub
    End If

    av = Int(RK / 2) ' Unidades necesarias por reparación

    If TC < av Then
        MSG$ = "REPARACION INACTIVA\NECESITAS " + Str$(av - TC) + " UNIDADES DE ENERGIA\\Se recarga al usar la unidad warp"
        ShowHelp MSG$
        WaitClick
        ClearScr 4
        Exit Sub
    End If

    If C$ <> "D" Then
        MSG$ = "DEBES ANCLAR EN UNA BASE PARA HACER REPARACIONES"
        ShowHelp MSG$
        WaitClick
        ClearScr 4
        Exit Sub
    End If

    BaseDamage
    PutLine 100, 2, 255, 255, 255, "REPARACION DE AVERIAS"

    HDR$ = "REPARACIONES"
    MSG$ = ""

    dm = 0
    I = 1
    For a = 1 To 9 ' Recorre todos los componentes y muestra sólo los averiados
        If D(a) < 0 Then
            kv(I) = a
            dm = 1
            Locate 6 + (I * 2), 50
            Print I;
            Locate 6 + (I * 2), 75
            Print D$(a)
            Locate 6 + (I * 2), 250
            Print "AVERIADO"
            I = I + 1
        End If
    Next a

    _Font mainfont&
    PutLine 90, 36, 255, 255, 255, "Digita el numero de dispositivo"
    PutLine 90, 38, 255, 255, 255, "Podes hacer hasta " + Str$(av - TC + 1) + " reparacion(es)"
    _Display

    ' Toma númerode componente a reparar, de teclado virtual o físico
    j = I
    k = I
    r = 1
    While j >= k
        k$ = ShowVnp$(1, r)
        If E = 11 Then
            ClearScr 4
            Exit Sub
        End If

        j = Val(k$)
        r = 0
        If j >= k Then
            SaveScr 5
            ShowHelp "'" + k$ + "' NO ES UNA OPCION VALIDA"
            WaitClick
            ClearScr 5
        End If
    Wend

    ClearScr 1
    ShowHelp D$(kv(j)) + " REPARADO"
    D(kv(j)) = 0
    TC = TC - 1
    WaitClick
    ClearScr 4
End Sub

' --------------------------------------------------------------------------------
' -- Calcula la distancia de la nave al enemigo (pitagoras)
' --------------------------------------------------------------------------------
Function DT
    Shared K(), I, S1, S2

    DT = Sqr((K(I, 1) - S1) ^ 2 + (K(I, 2) - S2) ^ 2)
End Function

' --------------------------------------------------------------------------------
' -- Numero aleatorio entre 1 y 7
' --------------------------------------------------------------------------------
Function RD
    RD = Int(Rnd * 7) + 1
End Function

' --------------------------------------------------------------------------------
' -- Redondear a dos decimales
' --------------------------------------------------------------------------------
Function P2 (X)
    P2 = Int(X * 100) / 100
End Function

' --------------------------------------------------------------------------------
' -- Redondear a 1 decimal
' --------------------------------------------------------------------------------
Function P3 (X)
    P3 = Int(X * 10) / 10
End Function

' --------------------------------------------------------------------------------
' -- Cargar archivos externos
' --------------------------------------------------------------------------------

' $include:'Lib\INIFile.bm' ' Leer / modificar archivos de configuracion
' $include:'Lib\Math.bm'    ' Rutinas matemticas no contempladas en el lenguaje
' $include:'Interface.bm'   ' Procesos de interfaz grafica
