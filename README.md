# SpaceWar7

Juego de estrategia por turnos

# Empezando

Estos son los fuentes en qBasic de un juego del año del moco remozado para que sea jugable hoy. Los fuentes están muy documentados para que un programador novato se haga una idea de que está pasando.
Si sólo querés jugar al juego, lo encontrás en [gitlab.com](https://gitlab.com/profugo/sw7-bin), hay versión para Linux y para windowss.

## Instalación
Sólo poné esto con tus fuentes qBasic. Acá se incluyen las fuentes del manual, para ver que usar para instalar fijate en los [BINARIOS](https://gitlab.com/profugo/sw7-bin)

## Uso
Si lo bajaste como zip descomprimilo donde quieras y abrí los fuentes con el IDE QB64

## Archivos
Estos son los fuentes a abrir en el IDE

* *sw7.bas*: Programa principal
* *Interface.bm*: Módulo principal de interfaz gráfica
* *Lib/INIFile.bm*: Módulo del gestor de archivos de configuración
* *Lib/Math.bm*: Extensi{on de funciones matemáticas

* *SW7_orig.BAS*: Conversión a qBasic del original para TI-99/4A

## Soporte
Un valiente no precisa ayuda, es decir, no hay

## Licencia
Este proyecto fué liberado bajo licencia [GNU GPL](https://www.gnu.org/licenses/licenses.es.html#GPL), bueh, lo que yo hice al menos, el resto:

### Sonidos
* over.ogg: [GFX Sounds](https://www.youtube.com/watch?v=9IcGDIk4i8s)
* Música: [FMA](https://freemusicarchive.org/music/Damiano_Baldoni/Old_Beat/unreachable)
* Las otras voces las hice yo

### Gráficos
* nave.png,  krinng.png: [Open Game Art](https://opengameart.org/content/top-down-space-ships)
* Imágen de portada: [Art Station](https://www.artstation.com/artwork/0nOewE)
* Los otros gráficos son invento mio

### Fuentes:
* Airbore GP: [1001 Fonts](https://www.1001fonts.com/airborne-gp-font.html)
* Orbitron Medium: [Dafont Free](https://www.dafontfree.net/orbitron-medium/f92274.htm)

## Estado del proyecto
Terminando este README
